# Screenshot LightDM login screen


### Introduction

A simple script to make screenshots of lightdm login screen, without using a Virtual Machine, or Xephyr.
The script should be reasonably easy to adapt to others X display managers such as GDM.


### Requirements

All you need to use this script is imagemagick package installed, and a sudoer user.

The script has been tested on Debian only, but should work with Debian derivatives and possibly other distos that meet the requirements.
But if it doesn't work in your environnement (if you don't use LightDM for example), or if you have specific needs, feel free to modify the script (It's free, as in freedom of speech). I obviously can't cover all the environments with a single script ).

Since I wrote it and tested on Debian, the script assumes that LightDM is running on virtual terminal (vt) 7, if it's not the case for you, please change line 8 (chvt 7) accordingly.


### Usage

Put the screen in your ~/ directory, then logout from your LightDM session and press Ctrl + Alt + F1 (or any other enbaled VT) and log in as normal (non-root, but sudoer) user in the tty, then execute the script from your home directory, and wait for the beep before loging in again in your LightDM session. Obviously, the script will automagically change to vt7 before taking the screenshot, you don't need to press Ctrl + Alt + F7, neither before the screenshot, nor after (in order to login).

Don't forget the $USER argument, since the script needs to be executed with sudo, $USER environnement variable is used to copy the screenshot in your ~/Images/Screenshots directory, at to assign the right owner to the .png file

Screenshots directory is created without generating errors, if it doesn't exist (mkdir -p)

````
me@linuxbox:~$ sudo ./screenshot.sh $USER
````

Enjoy

Once you confirmed the script worked for you, don't forget to logout from vt1, or whetever vt you used to log in, using tty N. otherwose your computer will obviously complain when you'll shut it down from your LightDM session.


### Screenshot example  
  
Here's a screenshot example obtained with this script, the image has been downsized to 800x450px in order to take both less space on screen for demo purposes, and less memory on the git repo. Obviously the script DOESN'T resize the captured image, the original one depends on your screen resolution.

<p align="center">
<img src="images/loginscreen.png" alt="My LightDM login screen">
</p>


### Notes about the repo name

I named this repo "lighdm_loginscreen-scrot" just because it "lighdm_loginscreen-screenshot" is a shitty name, but this script have nothing to do with the CLI screenshot tool "scrot", scrot is not used by the script. It's just an easy way to avoid using "screen" twice and make the name easier to pronounce while keeping the concept of "screenshot" in the repo name.
