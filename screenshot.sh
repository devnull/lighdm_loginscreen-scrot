#!/bin/sh

echo "Defining a username value for chown"
owner=$1
echo $owner 
sleep 1s

chvt 7
sleep 3s
DISPLAY=:0 XAUTHORITY=/var/run/lightdm/root/:0 xwd -root -out ~/login.xwd
convert ~/login.xwd ~/loginscreen.png
rm ~/login.xwd

mkdir -p /home/$owner/Images/Screenshots
mv ~/loginscreen.png /home/$owner/Images/Screenshots/loginscreen.png
chown $owner:$owner /home/$owner/Images/Screenshots/loginscreen.png

exit 0
